package com.jago.data;

import com.google.android.gms.maps.model.LatLng;

public abstract class Constants {
	//Main activity
	/**
	 * Events to show in main screen/next event
	 */
	public static final int NEXT_EVENTS = 3;
	
	//Event activity
	public static final LatLng VILLABONA = new LatLng(43.18756774029365, -2.05304457142643);
	
	/**
	 * Default camera zoom
	 */
	public static final int zoom = 17;
	
	//Data reader
	
	public static final int DURATION_DEFAULT = 1500; // 0H15m00
	
	
}
