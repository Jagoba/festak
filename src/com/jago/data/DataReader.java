package com.jago.data;
/**
 * @author Jagoba Gascon
 */
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.jago.jaiak.R;

public class DataReader  {

	private Document doc;

	public DataReader(Context c){
		InputStream i = c.getResources().openRawResource(R.raw.villabona_festak);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			this.doc = builder.parse(new InputSource(i));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Event getAfter(String id, String lang) {
		Node a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {//Lortu unekoa baino handiagoa duena id-a
			a = (Node) xPath.evaluate("//event[@id > "+ id +"]", doc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (a != null) {
			return createEventFromNode(lang, a);
		}
		return null;
	}

	public Event getEventById(String id, String lang){
		Node a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			a = (Node) xPath.evaluate("//event[@id >= "+ id +"]", doc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (a != null) {
			return createEventFromNode(lang, a);
		}
		return null;
	}

	public String getProgramTitle(String lang){
		String a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			a = (String) xPath.evaluate("program/title/" + lang + "/text()", doc, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return a;
	}

	public Vector<Event> getDayEvents(String lang, int day) {
		Vector<Event> s = new Vector<Event>();

		NodeList l = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			l = (NodeList) xPath.evaluate("//day[@number = "+ day + "]/event", doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (l != null) {
			for (int i = 0; i < l.getLength(); i++) {
				s.add(createEventFromNode(lang, l.item(i)));
			}
		}


		Collections.sort(s, new Comparator<Event>() {
			@Override
			public int compare(Event lhs, Event rhs) {
				if (Long.valueOf(lhs.getId()) > Long.valueOf(rhs.getId()))
					return 1;
				if (Long.valueOf(lhs.getId()) < Long.valueOf(rhs.getId()))
					return -1;
				return 0;
			}
		});
		return s;
	}

	public Vector<Day> getDays(){
		Vector<Day> vd = new Vector<Day>();
		NodeList a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			a = (NodeList) xPath.evaluate("//day", doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		int day = -1;
		int weekday = -1;
		int month = -1;
		String title_es = "", title_eu ="";
		if (a != null) {
			for (int i = 0; i < a.getLength(); i++) {
				title_es = "";
				title_eu ="";
				for (int j = 0; j < a.item(i).getAttributes().getLength(); j++) {
					if(a.item(i).getAttributes().item(j).getNodeName().compareTo("number") == 0)
						day = Integer.valueOf(a.item(i).getAttributes().item(j).getNodeValue());
					if(a.item(i).getAttributes().item(j).getNodeName().compareTo("weekday") == 0)
						weekday = Integer.valueOf(a.item(i).getAttributes().item(j).getNodeValue());
					if(a.item(i).getAttributes().item(j).getNodeName().compareTo("month") == 0)
						month = Integer.valueOf(a.item(i).getAttributes().item(j).getNodeValue());
					if(a.item(i).getAttributes().item(j).getNodeName().compareTo("title-es") == 0)
						title_es = a.item(i).getAttributes().item(j).getNodeValue();
					if(a.item(i).getAttributes().item(j).getNodeName().compareTo("title-eu") == 0)
						title_eu = a.item(i).getAttributes().item(j).getNodeValue();
				}
				Day d = new Day(weekday, day, month);
				d.setTitle_es(title_es);
				d.setTitle_eu(title_eu);
				vd.add(d);
			}
		}
		return vd;
	}

	public Vector<Event> getEventsByTag(String tag, String lang){
		Vector<Event> s = new Vector<Event>();

		NodeList l = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			l = (NodeList) xPath.evaluate("//event[contains(tags,'"+tag+"')]", doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (l != null) {
			for (int i = 0; i < l.getLength(); i++) {
				s.add(createEventFromNode(lang, l.item(i)));
			}
		}


		Collections.sort(s, new Comparator<Event>() {
			@Override
			public int compare(Event lhs, Event rhs) {
				if (Long.valueOf(lhs.getId()) > Long.valueOf(rhs.getId()))
					return 1;
				if (Long.valueOf(lhs.getId()) < Long.valueOf(rhs.getId()))
					return -1;
				return 0;
			}
		});
		return s;
	}

	public String[] getTagList(){
		String a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			a = (String) xPath.evaluate("program/tag-list/text()", doc, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}

		return a.split(",");
	}


	@SuppressLint("SimpleDateFormat")
	public Event getNextEvent(String lang){
		//String hour = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		String month = new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());
		String year = new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());
		String day = new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
		String hour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		String minute = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
		String s = year + month + day + hour + minute + "00";
		long id = Long.valueOf(s);
		id -= Constants.DURATION_DEFAULT;//Duration lortu, eta ez badauka <--- Hala ere kontuz... beste zerbait has daiteke, bat oraindik amaitu gabe dagoenean ere.
		Node a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			a = (Node) xPath.evaluate("//event[@id >= "+ id +"]", doc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (a != null) {
			return createEventFromNode(lang, a);
		}
		return null;
	}

	private Event createEventFromNode(String lang, Node a) {
		Event event;
		String d = getIdFromEventNode(a);
		event = new Event(d);
		int length = a.getChildNodes().getLength();
		for (int i = 0; i < length; i++) {
			Node n = a.getChildNodes().item(i);
			if (n.getNodeName().compareTo("title") == 0) {
				event.setTitle(getLangTypeValue(n, lang));
			}
			else if (n.getNodeName().compareTo("hour") == 0) {
				event.setHour(n.getTextContent());
			}
			else if (n.getNodeName().compareTo("minute") == 0) {
				event.setMinute(n.getTextContent());
			}
			else if (n.getNodeName().compareTo("description") == 0) {
				event.setDescription(getLangTypeValue(n, lang));
			}
			else if (n.getNodeName().compareTo("location") == 0) {
				event.setLocation(getLangTypeValue(n, lang));
			}
			else if (n.getNodeName().compareTo("coords") == 0) {
				NodeList nl = n.getChildNodes();
				for (int j = 0; j < nl.getLength(); j++) {
					if (nl.item(j).getNodeName().compareTo("lat") == 0) {
						String tc = nl.item(j).getTextContent();
						if (tc.length() > 0)
							event.setLat(Double.valueOf(tc));
						else
							event.setLat(0.0);
					}
					if (nl.item(j).getNodeName().compareTo("lng") == 0) {
						String tc = nl.item(j).getTextContent();
						if (tc.length() > 0)
							event.setLng(Double.valueOf(tc));
						else
							event.setLng(0.0);
					}
				}
			}
		}
		int l = a.getParentNode().getAttributes().getLength();
		Node k = a.getParentNode();
		for (int i = 0; i < l; i++) {
			if (k.getAttributes().item(i).getNodeName().compareTo("number") == 0)
				event.setDay(Integer.valueOf(k.getAttributes().item(i).getNodeValue()));
			if (k.getAttributes().item(i).getNodeName().compareTo("weekday") == 0)
				event.setWeekday(Integer.valueOf(k.getAttributes().item(i).getNodeValue()));
			if (k.getAttributes().item(i).getNodeName().compareTo("month") == 0)
				event.setMonth(Integer.valueOf(k.getAttributes().item(i).getNodeValue()));
		}

		return event;
	}

	/*
	@SuppressLint("SimpleDateFormat")
	private Node getNextEventNode(String lang){
		//String hour = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		String month = new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());
		String year = new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());
		String day = new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
		String hour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		String minute = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
		String s = year + month + day + hour + minute;
		long id = Long.valueOf(s);
		id -= Constants.DURATION_DEFAULT;
		Node a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			a = (Node) xPath.evaluate("//event[@id >= "+ id +"]", doc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return a;
	}*/


	/**
	 * Gets the value of the node. This node has n kids (one per language).
	 * @param n The node to search
	 * @param lang the language to search
	 * @return The value of the "lang" child node of n
	 */
	private String getLangTypeValue(Node n, String lang){
		String a = "";
		for (int i = 0; i < n.getChildNodes().getLength(); i++) {
			if (n.getChildNodes().item(i).getNodeName().compareTo(lang) == 0) {
				a = n.getChildNodes().item(i).getTextContent();
			}
		}
		return a;
	}

	private String getIdFromEventNode(Node k) {
		int l = k.getAttributes().getLength();
		for (int i = 0; i < l; i++) {
			if (k.getAttributes().item(i).getNodeName().compareTo("id") == 0)
				return k.getAttributes().item(i).getNodeValue();
		}
		return "";
	}

	public String getDayTitle(String day, String month, String weekday, String lang) {
		Node a = null;
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			a = (Node) xPath.evaluate("//day[@number = "+ day +" and @weekday = "+ weekday +" and @month = "+ month +"]", doc, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		if (a != null) {
			for (int i = 0; i < a.getAttributes().getLength(); i++) {
				if (a.getAttributes().item(i).getNodeName().compareTo("title-"+lang) == 0)
					return a.getAttributes().item(i).getNodeValue();
			}
		}
		return null;
	}
}