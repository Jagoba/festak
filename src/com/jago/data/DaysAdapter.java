package com.jago.data;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;



public class DaysAdapter extends BaseAdapter {

	private ArrayList<LinearLayout> items;

	private Context mContext;

	public DaysAdapter(Context c) {
		mContext = c;
		items = new ArrayList<LinearLayout>();
	}

	public int getCount() {
		return items.size();
	}

	public Object getItem(int position) {
		return items.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {         
		return (View) getItem(position);     
	} 
	
	public void addItem(LinearLayout ll){
		items.add(ll);
	}

}