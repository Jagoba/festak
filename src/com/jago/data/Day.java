package com.jago.data;

import java.util.Vector;

public class Day {
	
	public Vector<Event> events;
	public int number,day, month;
	private String title_es = null;
	private String title_eu = null;
	
	public Day(){
		events = new Vector<Event>();
		number = -1;
		day = -1;
		month = -1;
	}

	
	public Day(int day, int num, int m){
		events = new Vector<Event>();
		number = num;
		this.day = day;
		month = m;
	}
	
	public Day(int day, int num, int m, Vector<Event> e){
		events = e;
		number = num;
		this.day = day;
		month = m;
	}


	/**
	 * @return the title
	 */
	public String getTitle_es() {
		return title_es;
	}


	/**
	 * @param title the title to set
	 */
	public void setTitle_es(String title) {
		this.title_es = title;
	}


	/**
	 * @return the title_eu
	 */
	public String getTitle_eu() {
		return title_eu;
	}


	/**
	 * @param title_eu the title_eu to set
	 */
	public void setTitle_eu(String title_eu) {
		this.title_eu = title_eu;
	}

}
