package com.jago.jaiak;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jago.data.Constants;
import com.jago.data.DataReader;
import com.jago.data.Event;

@SuppressLint("SimpleDateFormat")
public class EventActivity extends MultiLanguageActivity {

	private String id;
	private Event event;
	private Context mContext = this;
	private String hour, minute, today, monthToday;
	private double latitude = 0.0, longitude = 0.0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);
		setupActionBar();
		id = getIntent().getStringExtra("id");

		DataReader r = new DataReader(this);
		event = r.getEventById(id, lang);

		if (event == null) {
			Intent i =  new Intent(mContext, MainScreen.class);
			startActivity(i);
			finish();
		}
		this.setTitle(event.getTitle());
		((TextView)findViewById(R.id.eventLayoutEventTitleText)).setText(event.getTitle());
		((TextView)findViewById(R.id.eventLayoutEventTimeText)).setText(event.getHour() + ":" + event.getMinute());		
		if (event.getDescription().length() == 0){
			((LinearLayout)((TextView)findViewById(R.id.eventLayoutEventDescriptionText)).getParent()).removeView(findViewById(R.id.eventLayoutEventDescriptionText));
		}
		else
			((TextView)findViewById(R.id.eventLayoutEventDescriptionText)).setText(event.getDescription());

		if (event.getLocation().length() == 0) {
			((LinearLayout)((LinearLayout)findViewById(R.id.locationTextView)).getParent()).removeView(findViewById(R.id.locationTextView));
		}
		else
			((TextView)findViewById(R.id.eventLayoutEventLocation)).setText(event.getLocation());
		
		latitude = event.getLat();
		longitude = event.getLng();
		if (latitude == 0.0 && longitude == 0.0) {
			View v = findViewById(R.id.eventLayoutGetDirections);
			((LinearLayout)((LinearLayout) v ).getParent()).removeView(v);
			v = findViewById(R.id.openMapLayout);
			((RelativeLayout)((FrameLayout) v).getParent()).removeView(v);
		}
		else {

			((LinearLayout)findViewById(R.id.eventLayoutGetDirections)).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					String label = event.getLocation();
					String uriBegin = "geo:" + latitude + "," + longitude;
					String query = latitude + "," + longitude;
					String encodedQuery = Uri.encode(query);
					String uriString = uriBegin + "?q=" + encodedQuery + "&z=" + String.valueOf(Constants.zoom);
					Uri uri = Uri.parse(uriString);
					Log.d("TAG",uriString);
					Intent intent = new Intent(Intent.ACTION_VIEW, uri);
					PackageManager pm = getPackageManager();
					if (!pm.queryIntentActivities(intent, PackageManager.GET_ACTIVITIES).isEmpty()){
						mContext.startActivity(intent);
					}
					else
						Toast.makeText(mContext, getResources().getString(R.string.mapsNotInstaled, "Google Maps"), Toast.LENGTH_LONG).show();
				}
			});

			((LinearLayout)findViewById(R.id.openMap)).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent i =  new Intent(mContext, EventMapActivity.class);
					i.putExtra("lat", latitude);
					i.putExtra("lng", longitude);
					i.putExtra("title", event.getTitle());
					i.putExtra("location", event.getLocation());
					startActivity(i);
				}
			});
		}
		Event hurrengoa = r.getNextEvent(lang);

		hour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		minute = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
		today =  new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
		monthToday =  new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());
		String year =  new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());
		String dateId = year+monthToday+today+hour+minute+"00";

		if (hurrengoa!=null && hurrengoa.getId().substring(0, ID_LENGTH-2).compareTo(event.getId().substring(0, ID_LENGTH-2)) == 0) {
			LinearLayout l = (LinearLayout) findViewById(R.id.eventActivityIsCurrentLayout);
			TextView next = (TextView) getLayoutInflater().inflate(R.layout.day_events_list_next, l, false);

			
			if (Double.valueOf(hurrengoa.getId()) - Double.valueOf(dateId) > Constants.DURATION_DEFAULT){
				next.setText(getResources().getString(R.string.next).toUpperCase(Locale.ENGLISH));
			}
			else {
				next.setText(getResources().getString(R.string.current).toUpperCase(Locale.ENGLISH));
			}
			LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			llp.setMargins(0, 0, 10, 10); // llp.setMargins(left, top, right, bottom);
			next.setLayoutParams(llp);
			next.setPadding(5, 5, 5, 5);
			l.addView(next, 0);
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.common_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_settings:
			settingsButtonAction();
			return true;
		case R.id.action_legal_notices:
			legalNoticesButtonAction();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void legalNoticesButtonAction(){
		Intent i = new Intent(this, LegalNotice.class);//legal notices class
		startActivity(i);
	}

	private void settingsButtonAction() {
		Intent i = new Intent(this, Settings.class);
		startActivity(i);
		//overridePendingTransition(R.anim.slide_in_right, R.anim.go_a_little_back);
	}

}
