package com.jago.jaiak;

import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesUtil;

public class LegalNotice extends MultiLanguageActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_legal_notice);
		TextView legal = (TextView) findViewById(R.id.legalText);
		String text = GooglePlayServicesUtil.getOpenSourceSoftwareLicenseInfo(this);
		if (text != null){
			legal.setText(text);
		}
		else {
			legal.setText("");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

}
