package com.jago.jaiak;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jago.data.Constants;

public class EventMapActivity extends MultiLanguageActivity {

	private GoogleMap mMap;
	private int myLocationButtonId = 0x02;
	private AlertDialog dialog;
	private String title = "", location = "";
	private double latitude = 0.0, longitude = 0.0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setTheme(R.style.actionbarSemiTransparent);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_fragment);
		setupActionBar();
		latitude = this.getIntent().getDoubleExtra("lat", 0.0);
		longitude = this.getIntent().getDoubleExtra("lng", 0.0);
		title = this.getIntent().getStringExtra("title");
		location = this.getIntent().getStringExtra("location");
		if (title.length() > 0)
			this.setTitle(title);

		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		if (mMap != null) {
			mMap.getUiSettings().setMyLocationButtonEnabled(true);
			mMap.setMyLocationEnabled(true);
			mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			mMap.getUiSettings().setCompassEnabled(false);
			if (latitude != 0.0 && longitude != 0.0) {
				MarkerOptions mo = new MarkerOptions().position(new LatLng(latitude, longitude));
				if (location.length() > 0) {
					mo.title(location);
				}
				mMap.addMarker(mo);
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), Constants.zoom));
			}
			else {
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.VILLABONA, Constants.zoom));
			}


			if (((View)findViewById(myLocationButtonId)) != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
				RelativeLayout.LayoutParams llp = (android.widget.RelativeLayout.LayoutParams) ((View)findViewById(0x02)).getLayoutParams();
				int actionBarHeight = getActionBarHeight();
				llp.setMargins(20, actionBarHeight+20, 20, 20); // llp.setMargins(left, top, right, bottom);
				((View)findViewById(myLocationButtonId)).setLayoutParams(llp);
			}
		}

		initializeDialog();
	}


	private void initializeDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		// 2. Chain together various setter methods to set the dialog characteristics
		builder.setTitle(getResources().getString(R.string.choose_map_type_title)).setItems(R.array.map_types, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mMap != null){
					switch (which){
					case 0:
						mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
						break;
					case 1:
						mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
						break;
					case 2:
						mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
						break;
					case 3:
						mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
						break;
					}

				}
			}
		});

		// 3. Get the AlertDialog from create()
		dialog = builder.create(); 
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private int getActionBarHeight(){
		int actionBarHeight = 0;
		TypedValue tv = new TypedValue();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)){
				actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
			}
		}
		return actionBarHeight;
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_settings:
			settingsButtonAction();
			return true;
		case R.id.action_legal_notices:
			legalNoticesButtonAction();
			return true;
		case R.id.action_change_map_type:
			changeMapTypeButtonAction();
			return true;
		}		
		return super.onOptionsItemSelected(item);
	}

	private void changeMapTypeButtonAction(){
		dialog.show();
	}

	private void legalNoticesButtonAction(){
		Intent i = new Intent(this, LegalNotice.class);//legal notices class
		startActivity(i);
	}

	private void settingsButtonAction() {
		Intent i = new Intent(this, Settings.class);
		startActivity(i);
		//overridePendingTransition(R.anim.slide_in_right, R.anim.go_a_little_back);
	}

}
