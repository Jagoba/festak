package com.jago.jaiak;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jago.data.Constants;
import com.jago.data.DataReader;
import com.jago.data.Event;

public class MainScreen extends MultiLanguageActivity {

	FrameLayout fl;
	LinearLayout helpScreen;
	Context c = this;

	private Event current, next;//Helpers for the onclicklisteners...

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

		// second argument is the default to use if the preference can't be found
		Boolean welcomeScreenShown = mPrefs.getBoolean("welcomeShown", false);

		if (!welcomeScreenShown) {
			// here you can launch another activity if you like
			// the code below will display a popup
			fl = new FrameLayout(this);
			setContentView(fl);
			ScrollView main = (ScrollView) getLayoutInflater().inflate(R.layout.main_screen, fl, false);
			fl.addView(main);

			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
				helpScreen  = (LinearLayout) getLayoutInflater().inflate(R.layout.help_screen_ics, fl, false);
			}
			else {
				helpScreen  = (LinearLayout) getLayoutInflater().inflate(R.layout.help_screen_less_than_ics, fl, false);
			}
			fl.addView(helpScreen);

			helpScreen.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					fl.removeView(helpScreen);	
					SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(c);
					SharedPreferences.Editor editor = mPrefs.edit();
					editor.putBoolean("welcomeShown", true);
					editor.commit(); // Very important to save the preference
				}
			});
			//pantalla de bienvenida!!!


		}
		else
			setContentView(R.layout.main_screen);

		DataReader r = new DataReader(getApplicationContext());	

		// Set title
		String tit = r.getProgramTitle(lang);
		if (tit != null) {
			this.setTitle(tit);
			((TextView) findViewById(R.id.mainScreenTitleTextView)).setText(tit); 
		}



		TextView now = (TextView) findViewById(R.id.textViewRightNow);
		TextView timeNow = (TextView) findViewById(R.id.textViewEventTime);
		View.OnClickListener l = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.buttonShowProgram:
					showProgramAction();
					break;
				case R.id.textViewRightNowTitle:
					openDay();
					break;
				}
			}
		};
		((TextView)findViewById(R.id.textViewRightNowTitle)).setOnClickListener(l);
		Button b = (Button) findViewById(R.id.buttonShowProgram);
		b.setOnClickListener(l);

		TextView after;
		current = r.getNextEvent(lang);
		if (current!= null){
			findViewById(R.id.rightNowLinearLayout).setOnClickListener(new OnClickListener() {
				private String id = current.getId();
				@Override
				public void onClick(View v) {
					openEvent(id);					
				}
			});
			now.setText(current.getTitle());
			timeNow.setText(current.getHour() + ":" + current.getMinute());
			LinearLayout nextEvent = (LinearLayout) findViewById(R.id.nextEventsLayout);
			after = (TextView) getLayoutInflater().inflate(R.layout.main_screen_next_events_text_view, null);
			next = r.getAfter(current.getId(), lang);
			String id = current.getId();
			boolean gehitua = false;
			for (int j = 0; j < Constants.NEXT_EVENTS; j++){
				after = (TextView) getLayoutInflater().inflate(R.layout.main_screen_next_events_text_view, null);
				next = r.getAfter(id, lang);
				if (next!= null){
					id = next.getId();
					after.setText(next.getHour() + ":" + next.getMinute() + " " + next.getTitle());
					after.setSelected(true);///Marquee
					after.setOnClickListener(new OnClickListener() {
						private String id = next.getId();
						@Override
						public void onClick(View v) {
							openEvent(id);					
						}
					});
					gehitua = true;
				}
				else{
					if (!gehitua)
						findViewById(R.id.textViewAfterTitle).setVisibility(View.INVISIBLE);
					break;
					//after.setText(getResources().getString(R.string.no_more_events));
					//after.setClickable(false);
				}
				nextEvent.addView(after);
			}
		}
		else {
			now.setText(getResources().getString(R.string.its_over));
			findViewById(R.id.rightNowLinearLayout).setClickable(false);
			((LinearLayout)findViewById(R.id.textViewAfterTitle).getParent()).removeView(findViewById(R.id.textViewAfterTitle));
		}
		String hour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		String minute = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
		String today =  new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
		String monthToday =  new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());
		String year =  new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());
		String dateId = year+monthToday+today+hour+minute+"00";
		if (current != null) {
			if (Double.valueOf(current.getId()) - Double.valueOf(dateId) > Constants.DURATION_DEFAULT)
				((TextView)findViewById(R.id.textViewRightNowTitle)).setText(R.string.nextValue);
		}
		else
			((LinearLayout)findViewById(R.id.textViewRightNowTitle).getParent()).removeView(findViewById(R.id.textViewRightNowTitle));
		createTagList(r.getTagList());



	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		TextView tv = null;
		Typeface type = Typeface.createFromAsset(this.getAssets(),"fonts/Roboto-LightItalic.ttf");
		tv = ((TextView) findViewById(R.id.textViewAfterTitle));
		if (tv!=null)
			tv.setTypeface(type);
		tv = ((TextView) findViewById(R.id.textViewRightNowTitle));
		if (tv!=null)
			tv.setTypeface(type);
		tv = ((TextView) findViewById(R.id.textViewCategorySearchTitle));
		if (tv!=null)
			tv.setTypeface(type);
	}

	public void createTagList(String[] s){
		if (s == null){
			findViewById(R.id.categoryLayout).setVisibility(View.INVISIBLE);
			return;
		}
		LinearLayout list = (LinearLayout) findViewById(R.id.categoryList);
		LinearLayout list2 = (LinearLayout) findViewById(R.id.categoryList2);
		LinearLayout tagListItem;
		for (int i = 0; i< s.length; i++){

			TextView tv2;


			if (s[i].compareTo("kids") == 0){
				tagListItem = (LinearLayout) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_layout, list, false);
				tv2 = (TextView) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_tag_text_kids, list, false);
				list.addView(tagListItem);
				tv2.setText(getResources().getString(R.string.kids));
				tv2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						openTag("kids");

					}
				});
			}
			else if (s[i].compareTo("culture") == 0){
				tagListItem = (LinearLayout) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_layout, list, false);
				tv2 = (TextView) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_tag_text_culture, list, false);
				tagListItem = (LinearLayout) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_layout, list, false);
				list.addView(tagListItem);
				tv2.setText(getResources().getString(R.string.culture));
				tv2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						openTag("culture");

					}
				});
			}
			else if (s[i].compareTo("concert") == 0){
				tagListItem = (LinearLayout) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_layout, list2, false);
				tv2 = (TextView) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_tag_text_concert, list2, false);
				tagListItem = (LinearLayout) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_layout, list2, false);
				list2.addView(tagListItem);
				tv2.setText(getResources().getString(R.string.concert));
				tv2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						openTag("concert");

					}
				});
			}
			else{
				tagListItem = (LinearLayout) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_layout, list2, false);
				tv2 = (TextView) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_tag_text_sports, list2, false);
				tagListItem = (LinearLayout) getLayoutInflater().inflate(R.layout.main_screen_category_list_item_layout, list2, false);
				list2.addView(tagListItem);
				tv2.setText(getResources().getString(R.string.sports));
				tv2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						openTag("sports");

					}
				});
			}
			tagListItem.addView(tv2);
		}
	}

	private void openTag(String tag){
		Intent i = new Intent(this, TagActivity.class);
		i.putExtra("tag", tag);
		startActivity(i);
	}

	private void showProgramAction() {
		Intent i = new Intent(this, WeekActivity.class);
		startActivity(i);
	}


	/*
	private void currentEventClickAction(){
		Intent i = new Intent(this, JaiakMain.class);
		i.putExtra("eventId", current.getId());
		startActivity(i);
		Toast.makeText(this, current.getId(), Toast.LENGTH_LONG).show();
		//overridePendingTransition(R.anim.slide_in_right, R.anim.go_a_little_back);
	}
	 */
	private void settingsButtonAction(){
		Intent i = new Intent(this, Settings.class);
		startActivity(i);
		//overridePendingTransition(R.anim.slide_in_right, R.anim.go_a_little_back);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.common_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			settingsButtonAction();
			return true;
		case R.id.action_legal_notices:
			legalNoticesButtonAction();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	private void legalNoticesButtonAction(){
		Intent i = new Intent(this, LegalNotice.class);//legal notices class
		startActivity(i);
	}

	private void openDay(){
		if (current == null) return;
		Intent i = new Intent(this, DayActivity.class);
		i.putExtra("day", String.valueOf(current.getDay()));
		i.putExtra("weekday", String.valueOf(current.getWeekday()));
		i.putExtra("month", String.valueOf(current.getMonth()));
		startActivity(i);
		//Toast.makeText(this, current.getId(), Toast.LENGTH_LONG).show();
	}

	private void openEvent(String id){
		Intent i = new Intent(this, EventActivity.class);
		i.putExtra("id", id);
		startActivity(i);
		//Toast.makeText(this, current.getId(), Toast.LENGTH_LONG).show();
	}

}
