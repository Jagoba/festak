package com.jago.jaiak;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.jago.data.Constants;
import com.jago.data.DataReader;
import com.jago.data.Day;
import com.jago.data.Event;

public class DayActivity extends MultiLanguageActivity {

	DayCollectionPagerAdapter dayCollectionAdapter;
	ViewPager mViewPager;
	private int day, month;
	public static DataReader r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		r = new DataReader(this);
		super.onCreate(savedInstanceState);
		setupActionBar();
		setContentView(R.layout.day_adapter);
		dayCollectionAdapter = new DayCollectionPagerAdapter(getSupportFragmentManager());
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(dayCollectionAdapter);

		day = Integer.valueOf(getIntent().getStringExtra("day"));
		month = Integer.valueOf(getIntent().getStringExtra("month"));
		mViewPager.setCurrentItem(getIndexByDate(day, month), true);
		if (lang.compareTo("es") == 0)
			setTitle(dayCollectionAdapter.days.elementAt(getIndexByDate(day, month)).getTitle_es());				
		else
			setTitle(dayCollectionAdapter.days.elementAt(getIndexByDate(day, month)).getTitle_eu());
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				if (lang.compareTo("es") == 0)
					setTitle(dayCollectionAdapter.days.elementAt(arg0).getTitle_es());				
				else
					setTitle(dayCollectionAdapter.days.elementAt(arg0).getTitle_eu());
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	private int getIndexByDate(int day, int month){
		Iterator<Day> dIt = dayCollectionAdapter.days.iterator();
		int i = -1;
		while (dIt.hasNext()){
			Day d = (Day) dIt.next();
			i++;
			if (d.number == day && d.month == month)
				return i;
		}
		return 0;
	}

	private static void openEvent(Context c, String id){
		Intent i = new Intent(c, EventActivity.class);
		i.putExtra("id", id);
		c.startActivity(i);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			final ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.common_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_settings:
			settingsButtonAction();
			return true;
		case R.id.action_legal_notices:
			legalNoticesButtonAction();
			return true;
		}		
		return super.onOptionsItemSelected(item);
	}

	private void legalNoticesButtonAction(){
		Intent i = new Intent(this, LegalNotice.class);//legal notices class
		startActivity(i);
	}

	private void settingsButtonAction() {
		Intent i = new Intent(this, Settings.class);
		startActivity(i);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	public String getTabTitle(int month, int day, int weekday){
		/*
		if (lang.compareTo("es") == 0)
			return day + " de " + getResources().getStringArray(R.array.month_names)[month-1];
		else//eu
			return getResources().getStringArray(R.array.month_names)[month-1] + " " + day;
		 */
		return getResources().getStringArray(R.array.week_days_names)[weekday-1];
	}

	public class DayCollectionPagerAdapter extends FragmentStatePagerAdapter {

		public Vector<Day> days;

		public DayCollectionPagerAdapter(FragmentManager fm) {
			super(fm);
			days = r.getDays();
		}

		@Override
		public Fragment getItem(int i) {
			DayLayoutFragment fragment = new DayLayoutFragment();
			Bundle args = new Bundle();

			args.putInt(DayLayoutFragment.ARG_DAY, days.get(i).number);
			args.putInt(DayLayoutFragment.ARG_MONTH, days.get(i).month);
			args.putInt(DayLayoutFragment.ARG_WEEKDAY, days.get(i).day);
			args.putString(DayLayoutFragment.ARG_LANG, lang);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return days.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Day d = days.get(position);
			return getTabTitle(d.month, d.number, d.day);
		}
	}

	// Instances of this class are fragments representing a single
	// object in our collection.
	public static class DayLayoutFragment extends Fragment {

		public static final String ARG_DAY = "day";
		public static final String ARG_MONTH = "month";
		public static final String ARG_WEEKDAY = "weekday";
		public static final String ARG_LANG = "lang";

		private String hour;
		private String minute;
		private String today;
		private String monthToday;
		private int day, month, weekday;
		private String lang, title;

		private Event e;

		@Override
		public View onCreateView(LayoutInflater inflater,
				ViewGroup cont, Bundle savedInstanceState) {
			// The last two arguments ensure LayoutParams are inflated
			// properly.

			View rootView = inflater.inflate(
					R.layout.activity_day, cont, false);
			lang  = (String) getArguments().get(ARG_LANG);
			hour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
			minute = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
			today =  new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
			monthToday =  new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());


			day = (Integer) getArguments().get(ARG_DAY);
			month = (Integer) getArguments().get(ARG_MONTH);
			weekday = (Integer) getArguments().get(ARG_WEEKDAY);
			title = r.getDayTitle(String.valueOf(day), String.valueOf(month), String.valueOf(weekday), lang);
			TextView v = (TextView) rootView.findViewById(R.id.textViewDayNumber);
			v.setText(String.valueOf(day));
			v = (TextView) rootView.findViewById(R.id.textViewWeekday);

			v.setText(getResources().getStringArray(R.array.week_days_names)[weekday-1]);
			v = (TextView) rootView.findViewById(R.id.TextViewMonth);
			v.setText(getResources().getStringArray(R.array.month_names)[month-1]);

			Vector<Event> ev = r.getDayEvents(lang, day);
			Event hurrengoa = r.getNextEvent(lang);
			Iterator<Event> it = ev.iterator();
			LinearLayout ly = (LinearLayout) rootView.findViewById(R.id.tagElementsListLayout);

			if (title != null && title.compareTo("") != 0) {
				//Gehitu eguna!
				LinearLayout dayLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.tag_day_layout, ly, false);

				TextView dayText = (TextView) getActivity().getLayoutInflater().inflate(R.layout.tag_day_text_view, dayLayout, false);
				dayText.setText(title);

				dayLayout.addView(dayText);
				ly.addView(dayLayout);
			}
			//scrollView = (ScrollView) rootView.findViewById(R.id.ScrollView1);
			while (it.hasNext()){
				LinearLayout container =  (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.day_events_list_all_layout, ly, false);

				e = it.next();

				TextView next;
				LinearLayout newEvent;

				if (hurrengoa!=null && hurrengoa.getId().substring(0, ID_LENGTH-2).compareTo(e.getId().substring(0, ID_LENGTH-2)) == 0) {
					newEvent =  (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.day_events_list_comming_up_layout, container, false);
				}
				else{
					newEvent =  (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.day_events_list_layout, container, false);
				}
				newEvent.setOnClickListener(new OnClickListener() {
					private String id = e.getId();
					@Override
					public void onClick(View v) {
						openEvent(getActivity(), id);				
					}
				});
				TextView t =  (TextView) newEvent.findViewById(R.id.eventTitle);
				TextView time =  (TextView) getActivity().getLayoutInflater().inflate(R.layout.day_events_list_time, null);
				time.setText(e.getHour() + ":" + e.getMinute());
				t.setText(e.getTitle());
				if (hurrengoa!=null && hurrengoa.getId().substring(0, ID_LENGTH-2).compareTo(e.getId().substring(0, ID_LENGTH-2)) == 0) {
					LinearLayout eventLayoutHeader =  (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.day_events_header, container, false);
					LinearLayout left = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.header_left, eventLayoutHeader, false);
					LinearLayout right =  (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.header_right, eventLayoutHeader, false);
					next = (TextView) getActivity().getLayoutInflater().inflate(R.layout.day_events_list_next, right, false);


					left.addView(time);
					eventLayoutHeader.addView(left);
					LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					llp.setMargins(10, 0, 0, 10); // llp.setMargins(left, top, right, bottom);
					next.setLayoutParams(llp);
					next.setPadding(5, 5, 5, 5);
					right.addView(next);
					eventLayoutHeader.addView(right);
					container.addView(eventLayoutHeader);
					/*if (scrollToView == null)  {
						Toast.makeText(getActivity(), "Scrollview initialized", Toast.LENGTH_LONG).show();
						scrollToView = container;
					}*/
					String hour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
					String minute = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
					String today =  new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
					String monthToday =  new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());
					String year =  new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());
					String dateId = year+monthToday+today+hour+minute+"00";
					if (hurrengoa!=null && hurrengoa.getId().substring(0, ID_LENGTH-2).compareTo(e.getId().substring(0, ID_LENGTH-2)) == 0) {
						/*if (Integer.valueOf(e.getHour()) > Integer.valueOf(hour))
							next.setText(getResources().getString(R.string.next).toUpperCase(Locale.ENGLISH));
						else if (Integer.valueOf(e.getHour()) < Integer.valueOf(hour))
							next.setText(getResources().getString(R.string.current).toUpperCase(Locale.ENGLISH));
						else //=
							if (Integer.valueOf(e.getMinute()) > Integer.valueOf(minute))
								next.setText(getResources().getString(R.string.next).toUpperCase(Locale.ENGLISH));
							else
								next.setText(getResources().getString(R.string.current).toUpperCase(Locale.ENGLISH));
						 */

						if (Double.valueOf(hurrengoa.getId()) - Double.valueOf(dateId) > Constants.DURATION_DEFAULT){
							next.setText(getResources().getString(R.string.next).toUpperCase(Locale.ENGLISH));
						}
						else {
							next.setText(getResources().getString(R.string.current).toUpperCase(Locale.ENGLISH));
						}
					}
				}
				else  {
					container.addView(time);
				}
				if (e.getDescription().length() > 0) {
					TextView description = (TextView) newEvent.findViewById(R.id.description);
					description.setText(e.getDescription());
				}

				container.addView(newEvent);
				ly.addView(container);
				t.setSelected(true);
			}
			Typeface type = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Roboto-Light.ttf");
			setAppFont((ViewGroup) rootView, type);
			return rootView;
		}
	}

}
