package com.jago.jaiak;

import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public abstract class MultiLanguageActivity extends FragmentActivity {
	
	protected static int ID_LENGTH = 14;
	protected String lang;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		lang = setLanguage();
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		Typeface type = Typeface.createFromAsset(this.getAssets(),"fonts/Roboto-Light.ttf");
		setAppFont((ViewGroup) findViewById(android.R.id.content).getRootView(), type);
	}

	private String setLanguage(){
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"com.jago.jaiak_preferences", Context.MODE_PRIVATE);
		String languageToLoad = pref.getString("lang", "");
		if (languageToLoad.compareTo("") == 0){
			Editor editor = pref.edit();
			editor.putString("lang", "es");
			editor.commit();
			languageToLoad = "es";
		}
		Locale locale = new Locale(languageToLoad); 
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
		return languageToLoad;
	}

	public static final void setAppFont(ViewGroup mContainer, Typeface mFont) {
		if (mContainer == null || mFont == null) return;

		final int mCount = mContainer.getChildCount();

		// Loop through all of the children.
		for (int i = 0; i < mCount; ++i) {
			final View mChild = mContainer.getChildAt(i);
			if (mChild instanceof TextView)	 {
				// Set the font if it is a TextView.
				((TextView) mChild).setTypeface(mFont);
				((TextView) mChild).setSelected(true);
			}
			else if (mChild instanceof ViewGroup) {
				// Recursively attempt another ViewGroup.
				setAppFont((ViewGroup) mChild, mFont);
			}
		}
	}

}
