package com.jago.jaiak;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jago.data.DataReader;
import com.jago.data.Day;
import com.jago.data.DaysAdapter;
import com.jago.data.Event;

@SuppressLint("SimpleDateFormat")
public class WeekActivity extends MultiLanguageActivity {
	private Day d;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_week);
		// Show the Up button in the action bar.
		setupActionBar();
		this.setTitle(getResources().getString(R.string.title_activity_week));
		GridView gridview = (GridView) findViewById(R.id.gridView1);
		DaysAdapter adapter = new DaysAdapter(this);
		gridview.setAdapter(adapter);


		LinearLayout l;
		TextView zenbakia;
		TextView testua, testua2;
		DataReader r = new DataReader(getApplicationContext());
		Vector<Day> vd = r.getDays();
		Event e = r.getNextEvent(lang);
		Iterator<Day> it = vd.iterator();
		//Day d;
		while (it.hasNext()){
			d = it.next();
			l = (LinearLayout) getLayoutInflater().inflate(R.layout.week_day_linear_layout, gridview, false);
			testua = (TextView) getLayoutInflater().inflate(R.layout.week_day_text, l, false);
			testua2 = (TextView) getLayoutInflater().inflate(R.layout.week_day_title_text, l, false);

			if (e != null && e.getDay() == d.number && e.getMonth() == d.month)
				zenbakia = (TextView) getLayoutInflater().inflate(R.layout.week_day_number_today, l, false);
			else if(e != null && ((e.getDay() > d.number && e.getMonth() == d.month) || e.getMonth() > d.month))
				zenbakia = (TextView) getLayoutInflater().inflate(R.layout.week_day_number_past, l, false);
			else
				zenbakia = (TextView) getLayoutInflater().inflate(R.layout.week_day_number, l, false);

			zenbakia.setText(String.valueOf(d.number));
			testua.setText(getResources().getStringArray(R.array.week_days_names)[d.day-1]);
			if (lang.compareTo("es") == 0)
				testua2.setText(d.getTitle_es());
			else
				testua2.setText(d.getTitle_eu());
			l.addView(zenbakia);
			zenbakia.setPadding(10, 10,10,10);

			l.addView(testua);
			l.setClickable(true);
			l.addView(testua2);
			adapter.addItem(l);

			//No se cambia la fuente de lo que hay dentro del adapter, hay que hacerlo a mano;
			Typeface mFont = Typeface.createFromAsset(this.getAssets(),"fonts/Roboto-Light.ttf");
			testua.setTypeface(mFont);
			testua.setSelected(true);
			testua2.setTypeface(mFont);
			testua2.setSelected(true);
			zenbakia.setTypeface(mFont);
			zenbakia.setSelected(true);

			zenbakia.setOnClickListener(new OnClickListener() {
				private int day = d.number;
				private int month = d.month;
				private int weekday = d.day;
				@Override
				public void onClick(View v) {
					click(day, month, weekday);
				}
			});
		}
	}


	private void click(int d, int m, int w){
		Intent i = new Intent(this, DayActivity.class);
		i.putExtra("day", String.valueOf(d));
		i.putExtra("weekday", String.valueOf(w));
		i.putExtra("month", String.valueOf(m));
		startActivity(i);
	}
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.common_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_settings:
			settingsButtonAction();
			return true;
		case R.id.action_legal_notices:
			legalNoticesButtonAction();
			return true;
		}		
		return super.onOptionsItemSelected(item);
	}

	private void legalNoticesButtonAction(){
		Intent i = new Intent(this, LegalNotice.class);//legal notices class
		startActivity(i);
	}

	private void settingsButtonAction() {
		Intent i = new Intent(this, Settings.class);
		startActivity(i);
		//overridePendingTransition(R.anim.slide_in_right, R.anim.go_a_little_back);
	}

}
