package com.jago.jaiak;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jago.data.Constants;
import com.jago.data.DataReader;
import com.jago.data.Event;

@SuppressLint("SimpleDateFormat")
public class TagActivity extends MultiLanguageActivity {
	private String tag, hour, minute, today, monthToday,originalTag;
	private View scrollView = null;
	private boolean scrolled = false;
	private Event e;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		originalTag = getIntent().getStringExtra("tag");
		if (originalTag.compareTo("kids") == 0){
			setTheme(R.style.AppBaseTheme_actionbarRed);
		}
		else if (originalTag.compareTo("culture") == 0){
			setTheme(R.style.AppBaseTheme_actionbarGreen);
		}
		else if (originalTag.compareTo("concert") == 0){
			setTheme(R.style.AppBaseTheme_actionbarOrange);
		}
		else{//sports
			setTheme(R.style.AppBaseTheme_actionbarPurple);
		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tag);

		// Show the Up button in the action bar.
		setupActionBar();
		LinearLayout topBg = (LinearLayout) findViewById(R.id.tagActivityHeader);

		if (originalTag.compareTo("kids") == 0){
			tag = getResources().getString(R.string.kids);
			topBg.setBackgroundDrawable((Drawable) getResources().getDrawable(R.drawable.tag_kids_background));
		}
		else if (originalTag.compareTo("culture") == 0){
			tag = getResources().getString(R.string.culture);
			topBg.setBackgroundDrawable((Drawable) getResources().getDrawable(R.drawable.tag_culture_background));
		}
		else if (originalTag.compareTo("concert") == 0){
			tag = getResources().getString(R.string.concert);
			topBg.setBackgroundDrawable((Drawable) getResources().getDrawable(R.drawable.tag_concerts_background));
		}
		else{//sports
			tag = getResources().getString(R.string.sports);
			topBg.setBackgroundDrawable((Drawable) getResources().getDrawable(R.drawable.tag_sports_background));
		}
		setTitle(tag);

		hour = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
		minute = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
		today =  new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());
		monthToday =  new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());

		DataReader r = new DataReader(this);
		TextView v = (TextView) findViewById(R.id.TextViewTag);
		v.setText(tag);

		Vector<Event> ev = r.getEventsByTag(originalTag,lang);
		Event hurrengoa = r.getNextEvent(lang);
		Iterator<Event> it = ev.iterator();
		LinearLayout ly = (LinearLayout) findViewById(R.id.tagElementsListLayout);
		int currentDay = 0;
		if (ev.size() == 0){
			//Gehitu eguna!
			LinearLayout dayLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.tag_day_layout, ly, false);

			TextView dayText = (TextView) getLayoutInflater().inflate(R.layout.tag_day_text_view, dayLayout, false);
			dayText.setText(getResources().getString(R.string.no_more_events));

			dayLayout.addView(dayText);
			ly.addView(dayLayout);
		}
		else
			while (it.hasNext()){
				LinearLayout container =  (LinearLayout) getLayoutInflater().inflate(R.layout.day_events_list_all_layout, ly, false);
				e = it.next();


				if (currentDay != e.getDay()) {
					currentDay = e.getDay();
					//Gehitu eguna!
					LinearLayout dayLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.tag_day_layout, ly, false);
					Log.d("TAG", String.valueOf(e.getWeekday()));
					Log.d("TAG", String.valueOf(e.getDay()));
					TextView dayText = (TextView) getLayoutInflater().inflate(R.layout.tag_day_text_view, dayLayout, false);
					dayText.setText(String.valueOf(currentDay)+" "+ getResources().getStringArray(R.array.week_days_names)[e.getWeekday()-1]);

					dayLayout.addView(dayText);
					ly.addView(dayLayout);
				}
				TextView next;
				LinearLayout newEvent;
				if (hurrengoa!=null && hurrengoa.getId().substring(0, ID_LENGTH-2).compareTo(e.getId().substring(0, ID_LENGTH-2)) == 0) {
					newEvent =  (LinearLayout) getLayoutInflater().inflate(R.layout.day_events_list_comming_up_layout, container, false);
				}
				else{
					newEvent =  (LinearLayout) getLayoutInflater().inflate(R.layout.day_events_list_layout, container, false);
				}

				newEvent.setOnClickListener(new OnClickListener() {
					private String id = e.getId();
					@Override
					public void onClick(View v) {
						openEvent(id);				
					}
				});
				TextView t =  (TextView) newEvent.findViewById(R.id.eventTitle);
				TextView time =  (TextView) getLayoutInflater().inflate(R.layout.day_events_list_time, null);
				time.setText(e.getHour() + ":" + e.getMinute());
				t.setText(e.getTitle());
				if (hurrengoa!=null && hurrengoa.getId().substring(0, ID_LENGTH-2).compareTo(e.getId().substring(0, ID_LENGTH-2)) == 0) {
					LinearLayout eventLayoutHeader =  (LinearLayout) getLayoutInflater().inflate(R.layout.day_events_header, container, false);
					LinearLayout left = (LinearLayout) getLayoutInflater().inflate(R.layout.header_left, eventLayoutHeader, false);
					LinearLayout right =  (LinearLayout) getLayoutInflater().inflate(R.layout.header_right, eventLayoutHeader, false);
					next = (TextView) getLayoutInflater().inflate(R.layout.day_events_list_next, right, false);


					left.addView(time);
					eventLayoutHeader.addView(left);
					LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					llp.setMargins(10, 0, 0, 10); // llp.setMargins(left, top, right, bottom);
					next.setLayoutParams(llp);
					next.setPadding(5, 5, 5, 5);
					right.addView(next);
					eventLayoutHeader.addView(right);
					container.addView(eventLayoutHeader);
					scrollView = container;
					String year =  new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime());
					String dateId = year+monthToday+today+hour+minute+"00";
					if (Double.valueOf(hurrengoa.getId()) - Double.valueOf(dateId) > Constants.DURATION_DEFAULT){
						next.setText(getResources().getString(R.string.next).toUpperCase(Locale.ENGLISH));
					}
					else {
						next.setText(getResources().getString(R.string.current).toUpperCase(Locale.ENGLISH));
					}
				}
				else  {
					container.addView(time);
				}
				if (e.getDescription().length() > 0) {
					TextView description = (TextView) newEvent.findViewById(R.id.description);
					description.setText(e.getDescription());
				}

				container.addView(newEvent);
				ly.addView(container);
				t.setSelected(true);
			}
	}

	private void openEvent(String id){
		Intent i = new Intent(this, EventActivity.class);
		i.putExtra("id", id);
		startActivity(i);
		//Toast.makeText(this, current.getId(), Toast.LENGTH_LONG).show();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (scrollView != null && !scrolled) {
			scrolled = true;
			ScrollView sv = (ScrollView) findViewById(R.id.ScrollView1);
			sv.scrollTo(scrollView.getLeft(), scrollView.getTop());
		}
	}
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.common_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.action_settings:
			settingsButtonAction();
			return true;
		case R.id.action_legal_notices:
			legalNoticesButtonAction();
			return true;
		}		
		return super.onOptionsItemSelected(item);
	}

	private void legalNoticesButtonAction(){
		Intent i = new Intent(this, LegalNotice.class);//legal notices class
		startActivity(i);
	}

	private void settingsButtonAction() {
		Intent i = new Intent(this, Settings.class);
		startActivity(i);
		//overridePendingTransition(R.anim.slide_in_right, R.anim.go_a_little_back);
	}

}
